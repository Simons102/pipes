# Pipes

![pipes icon](icon.png)

## About

A flash puzzle game about connecting pipes, originally found on the LG KP500.  
I did not write this myself; the swf file is taken from a firmware upgrade image.  
This works in any modern browser with wasm support thanks to [ruffle](https://ruffle.rs).  
The only things I did were to write a bit of js to properly position and scale it (the game has a few black lines at the top where the KP500 top bar would be), and to slightly modify the swf with the awesome [JPEXS Free Flash Decompiler](https://github.com/jindrapetrik/jpexs-decompiler) to not crash when clicking the quit button and to be able to save game progress.  
(Ruffle puts this in webstorage, check out the storage/ local storage section of your favorite browser's web dev tools.)  

![screenshot](pipes_screenshot.png)
