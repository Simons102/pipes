
ruffleLink=$(cat ruffle_download_link.txt)

wget "$ruffleLink" -O "ruffle_selfhosted.zip"
if [ ! -f "ruffle_selfhosted.zip" ]; then
    echo "ruffle_selfhosted.zip does not exist"
    exit 1
fi

mkdir -p public/ruffle
unzip ruffle_selfhosted.zip -d public/ruffle
rm ruffle_selfhosted.zip

if [ ! -f "public/ruffle/ruffle.js" ]; then
    echo "missing ruffle.js"
    exit 1
fi

exit 0
